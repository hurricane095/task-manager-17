package ru.krivotulov.tm.command.system;

import ru.krivotulov.tm.command.AbstractCommand;

/**
 * VersionCommand
 *
 * @author Aleksey_Krivotulov
 */
public class VersionCommand extends AbstractCommand {

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Display application version.";

    public static final String ARGUMENT = "-v";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("1.17.0");
    }

}
