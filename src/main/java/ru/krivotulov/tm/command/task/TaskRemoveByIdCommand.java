package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskRemoveByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-id";

    public static final String DESCRIPTION = "Remove task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        getTaskService().deleteById(id);
    }

}
