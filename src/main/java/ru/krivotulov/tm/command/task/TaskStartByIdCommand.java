package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskStartByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskStartByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-id";

    public static final String DESCRIPTION = "Start task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        getTaskService().changeStatusById(id, Status.IN_PROGRESS);
    }

}
