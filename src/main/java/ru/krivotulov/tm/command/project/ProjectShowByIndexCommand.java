package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * ProjectShowByIndexCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-show-by-index";

    public static final String DESCRIPTION = "Show project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

}
