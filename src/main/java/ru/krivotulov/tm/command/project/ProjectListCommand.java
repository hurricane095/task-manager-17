package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

/**
 * ProjectListCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Display project list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.readLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projectList = getProjectService().findAll(sort);
        for (Project project : projectList) {
            System.out.println(project);
        }
    }

}
